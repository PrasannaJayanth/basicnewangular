import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { AppComponent } from './app.component';
import { WarningAlertComponent } from './warning-alert/warning-alert.component';
import { SuccessAlertComponent } from './success-alert/success-alert.component';
import { LoginformComponent } from './loginform/loginform.component';
import { SuceessformComponent } from './signform/signform.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginformContentComponent } from './loginform/loginform-content/loginform-content.component';
import { ForgotPasswordComponent } from './loginform/forgot-password/forgot-password.component';
import { FacebookSignComponent } from './signform/facebook-sign/facebook-sign.component';
import { NewPasswordComponent } from './loginform/forgot-password/new-password/new-password.component';
import { FormsComponent } from './forms/forms.component'
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  
  
  {path: 'loginFormContent' , component: LoginformContentComponent},
 
  { path: 'successform/:id/:string', component: SuceessformComponent },
  { path: 'forgot-password/:id', component: ForgotPasswordComponent },
  {
    path: 'loginForm', component: LoginformComponent,
  },
 {path: '', component: LoginformComponent}
 
];

@NgModule({
  declarations: [
    AppComponent,
    WarningAlertComponent,
    SuccessAlertComponent,
    LoginformComponent,
    SuceessformComponent,
NewPasswordComponent,
    LoginformContentComponent,
     ForgotPasswordComponent,
     FacebookSignComponent,
     FormsComponent,
    
   
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(routes)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
