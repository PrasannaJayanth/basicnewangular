import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
  OnSubmit(form: NgForm) {
    console.log(form);
  }

  coverLetter = '';

  newForm!: FormGroup ;

  ngOnInit(): void {
    this.newForm = new FormGroup({
      'userData': new FormGroup({
        'Username': new FormControl(null, Validators.required),
        'email': new FormControl(null, [Validators.required, Validators.email])
      }),
      'hobbies': new FormArray([]),

    })
  }
 

  OnSubmitForm() {
    console.log(this.newForm);
  }

  ViewHobbyInput() {
    const hobbie = new FormControl(null, Validators.required);
    (<FormArray>this.newForm.get('hobbies')).push(hobbie);
  }

  
}
