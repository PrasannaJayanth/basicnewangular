import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FacebookSignComponent } from './facebook-sign.component';

describe('FacebookSignComponent', () => {
  let component: FacebookSignComponent;
  let fixture: ComponentFixture<FacebookSignComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FacebookSignComponent]
    });
    fixture = TestBed.createComponent(FacebookSignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
