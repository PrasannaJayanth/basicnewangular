import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-suceessform',
  templateUrl: './signform.component.html',
  styleUrls: ['./signform.component.css']
})
export class SuceessformComponent {
  constructor(private route: Router, private route1: ActivatedRoute) {

  }
  toNavigate1() {
    this.route.navigate(['/loginForm'] , {relativeTo: this.route1});
  }

}
