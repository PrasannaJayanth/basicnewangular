import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuceessformComponent } from './signform.component';

describe('SuceessformComponent', () => {
  let component: SuceessformComponent;
  let fixture: ComponentFixture<SuceessformComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SuceessformComponent]
    });
    fixture = TestBed.createComponent(SuceessformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
