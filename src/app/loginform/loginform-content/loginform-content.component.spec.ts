import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginformContentComponent } from './loginform-content.component';

describe('LoginformContentComponent', () => {
  let component: LoginformContentComponent;
  let fixture: ComponentFixture<LoginformContentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginformContentComponent]
    });
    fixture = TestBed.createComponent(LoginformContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
