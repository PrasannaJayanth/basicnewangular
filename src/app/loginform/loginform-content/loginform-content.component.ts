import { Component, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-loginform-content',
  templateUrl: './loginform-content.component.html',
  styleUrls: ['./loginform-content.component.css']
})
export class LoginformContentComponent {
  constructor(private router: Router , private route: ActivatedRoute) {
    
  }
  @Input() username1: string = '';
  @Input() useremail1: string = '';
  OnChildToParent() {
    this.router.navigate(['/loginForm'], { relativeTo: this.route })
  }

  
 


  
}
