import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent {
  @Output() userDataEvent = new EventEmitter<{ pass: string }>();
  pass = ""

  passData() {
    this.userDataEvent.emit({ pass: this.pass });
  }
}
