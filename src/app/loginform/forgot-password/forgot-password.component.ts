import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent {
  constructor(private router: Router, private route: ActivatedRoute) {

  }
  OnChildToParent() {
    this.router.navigate(['/loginForm'], { relativeTo: this.route })
  }
  receivedUserData: { pass: string; } | undefined ;

  receiveUserData1(data: { pass: string }) {
    this.receivedUserData = data;
  }
}
